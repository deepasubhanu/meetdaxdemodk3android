package com.example.dis015.demodk;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


public class ProgressCircleActivity extends AppCompatActivity{

    private ProgressDialog progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.progress_circle_activity);

        progressBar = new ProgressDialog(ProgressCircleActivity.this);
        progressBar.setMessage("Please wait ...");
        progressBar.setCancelable(true);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.show();


        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                        Thread.sleep(5*1000);
                        Intent i=new Intent(ProgressCircleActivity.this,PhoneNumberLogin.class);
                        startActivity(i);
                        progressBar.dismiss();
                        //Remove activity
                        finish();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                progressBar.dismiss();

            }
            }).start();

    }
}
