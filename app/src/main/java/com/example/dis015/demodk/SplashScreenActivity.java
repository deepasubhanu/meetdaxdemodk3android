package com.example.dis015.demodk;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;


public class SplashScreenActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        /****** Create Thread that will sleep for 5 seconds *************/
        Thread background = new Thread() {
            public void run() {

                try {
                    // Thread will sleep for 5 seconds
                    sleep(4*1000);

                    // After 5 seconds redirect to another intent
                    Intent i=new Intent(SplashScreenActivity.this, ProgressCircleActivity.class);
                    startActivity(i);

                    //Remove activity
                    finish();

                }
                catch (Exception e) {

                }
            }
        };

        // start thread
        background.start();
    }

}

